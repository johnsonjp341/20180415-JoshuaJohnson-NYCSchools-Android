package com.example.johnsonjp34.myapplication;

/*Simple Single Page App that displays the NYC schools in a scrollable ListView after parsing an XML file.
Each school is clickable. After clicking you get a TextView set to SAT score data.

The app uses the standard Android XMLPullParser. Keep in mind that this is parsing info from a XML file.
In the event that this occurs on a larger scale OR by web access the app will need to use an ASYNCTASK to
properly take the thread load off the MainView thread.

Unit Tests are great, monitoring for proper GC is great, optimizing Big-O is good too.
With only a few hours to work this is all I got done. It works though.

I also parsed out the Database Identifiers that way a person could reference other tables on that NYC site
and populate an app with other interesting school info.


Thanks for the opportunity to interview!

*/

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

//Using the Android XML Parser
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import android.widget.ListAdapter;
import android.widget.TextView;

//favorite debugging import
import android.widget.Toast;

//Some people like to use arrays, but Lists work great in Android especially when connecting to Views
import java.util.List;



public class MainActivity extends AppCompatActivity {

    //XMLPullParser - no namespace
    private static final String ns = null;

    //Input Stream to feed the XML info to the Parser
    InputStream in;

    //Setup and separate info for each school by Lists.
    List<String> schoolNames;
    List<String> numberOfTakers;
    List<String> mathScore;
    List<String> writingScore;
    List<String> readingScore;
    List<String> dbnID;

    //TextView that display all info when a school is clicked.
    TextView additionalInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //objects...
        schoolNames = new ArrayList<String>();
        numberOfTakers = new ArrayList<String>();
        mathScore = new ArrayList<String>();
        readingScore = new ArrayList<String>();
        writingScore = new ArrayList<String>();
        dbnID = new ArrayList<String>();

        //ListView. This is scrollable to display a clickable list of schools.
        ListView tableView = (ListView) findViewById(R.id.schoollist);

        //Display the score at the bottom of the page after a click on the school name.
        additionalInfo =  (TextView)findViewById(R.id.scoreDetails);

        //Fire up the parser. And print to logcat all the problems.
        try {

            parse();

        } catch (XmlPullParserException e) {

            e.printStackTrace();
        }
        catch (IOException e) {

            e.printStackTrace();
        }

        //might be neater to split up into declaration and assignment.
        //Connect our list of schools to the listview
        final ListAdapter myListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, schoolNames);
        tableView = (ListView) findViewById(R.id.schoollist);
        tableView.setAdapter(myListAdapter);


        //Make the List Clickable
        tableView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View v, int arg2,
                                    long arg3) {
                //Strings that hold item at arg2 position in each List
                String whatYouClicked = (String) myListAdapter.getItem(arg2);
                String takers = (String) numberOfTakers.get(arg2);
                String mathStScore = (String) mathScore.get(arg2);
                String readingStScore = (String) readingScore.get(arg2);
                String writingStScore = (String) writingScore.get(arg2);
                String dbnStID = (String) dbnID.get(arg2);

                //Display the scores and additional info
               additionalInfo.setText(whatYouClicked + ", Number of takers: " + takers +"," + " Math Score: " + mathStScore + "," + " Reading Score: " + readingStScore + "," + " Writing Score: " + writingStScore + "," + " Database Identifier: " + dbnStID);

            }
        });

    }


        //XMLPullParser. This is the standard setup in the documentation
        public List parse() throws XmlPullParserException, IOException {
            try {

                XmlPullParser parser = Xml.newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
               in = getApplicationContext().getAssets().open("SATScores.xml");
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(in, null);
                parser.nextTag();
                return readFeed(parser);
            }
            finally {
                //better close it
                in.close();
            }

        }


    //working our way through the XML file. Starting with the outermost tags.
    private List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List entries = new ArrayList();

        parser.require(XmlPullParser.START_TAG, ns, "response");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Next Tag
            if (name.equals("row")) {

                entries.add(readEntry(parser));
            } else {
                skip(parser);
            }
        }
        return entries;
    }


    public static class Entry {
        public final String names;
        public final String link;
        public final String otherinfo;

        private Entry(String names, String otherinfo, String link) {
            this.names = names;
            this.otherinfo = otherinfo;
            this.link = link;
        }
    }


    private Entry readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {

        parser.require(XmlPullParser.START_TAG, ns, "row");

        String names = null;
        String otherinfo = null;
        //Filler String to temp hold info.
        String link = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("school_name")) {
                names = readSchoolName(parser);
                schoolNames.add(names);
                Log.d("SCHOOL NAME", names);

            } else if (name.equals("num_of_sat_test_takers")) {
                otherinfo = readNumOfTakers(parser);
                numberOfTakers.add(otherinfo);
                Log.d("Number of Takers", otherinfo);

            } else if (name.equals("sat_critical_reading_avg_score")) {
                link = readReading(parser);
                readingScore.add(link);
                Log.d("Reading Score", link);
            } else if (name.equals("sat_math_avg_score")) {
                link = readMath(parser);
                mathScore.add(link);
                Log.d("Math Score", link);
            }else if (name.equals("sat_writing_avg_score")) {
                link = readWriting(parser);
                writingScore.add(link);
                Log.d("Writing Score", link);
            }else if (name.equals("dbn")) {
                link = readDBN(parser);
                dbnID.add(link);
                Log.d("DBN", link);
            } else {
                skip(parser);
            }
        }

        return new Entry(names, otherinfo, link);
    }

    //feed parser with correct tags
    private String readSchoolName(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, ns, "school_name");
        String names = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "school_name");
        return names;
    }

    private String readReading(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, ns, "sat_critical_reading_avg_score");
        String names = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "sat_critical_reading_avg_score");
        return names;
    }

    private String readMath(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, ns, "sat_math_avg_score");
        String names = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "sat_math_avg_score");
        return names;
    }

    private String readWriting(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, ns, "sat_writing_avg_score");
        String names = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "sat_writing_avg_score");
        return names;
    }

    private String readDBN(XmlPullParser parser) throws IOException, XmlPullParserException {

        parser.require(XmlPullParser.START_TAG, ns, "dbn");
        String names = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "dbn");
        return names;
    }



    private String readNumOfTakers(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "num_of_sat_test_takers");
        String otherinfo = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "num_of_sat_test_takers");
        return otherinfo;
    }


    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }


   //THIS IS IMPORTANT. WITHOUT SKIP YOU GET EVERYTHING PARSED.
    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }





}
